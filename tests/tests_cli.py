from provisionVMs import cli

configdir = /path/to/configs

@pytest.fixture
def parser():
    return cli.create_parser()

def test_parser_without_data(parser):
    """
    Without a specified data directory the parser will exit
    """
    with pytest.raises(SystemExit):
        parser.parse_args(["--configs", configdir])

