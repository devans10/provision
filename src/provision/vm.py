from __future__ import print_function
from provision import connections

import time
import json
from six.moves.urllib import parse as urlparse

def get_id_from_name(s, baseUri, resource, obj_name):
    uri = "%s/%s/id" % (baseUri, resource)
    r = s.get(uri)
    for obj in r.json():
        if 'name' in obj.keys():
            if obj['name'] == obj_name:
                return obj['value']
    raise Exception('Failed to find id for {name}'.format(name=obj_name))

def wait_for_job(joburi, s):
    while True:
        time.sleep(1)
        r = s.get(joburi)
        job = r.json()
        if job['summaryDone']:
            print('{name}: {runState}'.format(name=job['name'], runState=job['jobRunState']))
            if job['jobRunState'].upper() == 'FAILURE':
                raise Exception('Job Failed: {error}',format(error=job['error']))
            elif job['jboRunState'].upper() == 'SUCCESS':
                if 'resultId' in job:
                    return job['resultId']

def update_virtual_disk(s, baseUri, id, newDiskName):
    print("VM Update VM Disk Mapping for %s" % id)
    print("######################################")

    uri = '{base}/VirtualDisk/{id}'.format(base=baseUri, id=id)
    r = s.get(uri)
    disk - r.json()
    disk['name'] = newDiskName

    r = s.put(uri, data=json.dumps(disk))
    job = r.json()
    wait_for_job(job['id']['uri'],s)

def clone_vm(s, baseUri, template, hostname, repository, serverpool):
    print("Clone VM Template %s to %s" % (template, hostname))
    print("#################################################")

    repo_id = get_id_from_name(s, baseUri, 'Repository', repository) 
    sp_id = get_id_from_name(s, baseUri, 'ServerPool', serverpool)
    template_id = get_id_from_name(s, baseUri, 'Vm', template)

    data = {}
    data['serverPoolId'] = sp_id
    data['repositoryId'] = repo_id
    data['createTemplate'] = False

    uri = '{base}/Vm/{vmId}/clone?{params}'.format(base=baseUri,vmId=template_id,params=urlparse.urlencode(data))
    r = s.put(uri)
    job = r.json()

    # wait for the job to complete
    vm_id = wait_for_job(job['id']['uri'], s)
    print("New VM Id: %s" % json.dumps(vm_id, indent=2))

    # change VM Name
    print("Change %s to %s" % (vm_id['name'], hostname))
    print("###########################################")

    data = {'name':hostname, 'id':vm_id}
    r = s.put('{base}/Vm/{vmId}'.format(base=baseUri, vmId=vm_id['value'], data=json.dumps(data)))
    job = r.json()
    wait_for_job(job['id']['uri'], s)

    # change VirtualDisk names
    uri = '{base}/Vm/{vmId}'.format(base=baseUri, vmId=vm_id['value'])
    r = s.get(uri)
    vm = r.json()

    dNum = 0
    for disk in vm['vmDiskMappingsIds']:
        dNum += 1
        if dNum == 1:
            newDiskName = vm['name']+'-xvda'
        else:
            newDiskName = vm['name']+'-xvdb'

        dMapping = s.get('{base}/VmDiskMapping/{diskId}'.format(base=baseUri, diskId=disk['value']))
        dm = dmapping.json()
        update_virtual_disk(s, baseUri, dm['virtualDiskId']['value'], newDiskName)

def main(vm):
    
    ovmconf = './configs/ovm.conf'
    s, baseUri = connections.ovm(ovmconf)

    
