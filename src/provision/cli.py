from argparse import ArgumentParser

def create_parser():
    parser = ArgumentParser(description="""
    Provision VMs from config files
    """)
    parser.add_argument("--configs", required=True, help="Local directory containing JSON config files")

    return parser

