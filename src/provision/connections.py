import sys
import time
import json
import requests

def ovm(ovmconf):
    with open(ovmconf) as f:
        props = json.load(f)

    ### Create Connection to OVM Manager ###
    s = requests.Session()
    s.auth = (props.username, props.password)
    s.verify = False
    s.headers.update({'Accept': 'application/json', 'Content-Type': 'application/json'})
    baseUri = 'https://%s:7002/ovm/core/wsapi/rest' % props.server

    try:
        while True:
            r = s.get(baseUri+'/Manager')
            manager = r.json()
            if manager[0]['managerRunState'].upper() == 'RUNNING':
                print("OVM Manager is %s" % manager[0]['managerRunState'])
                break
            time.sleep(1)
        return s, baseUri
    except:
        print("Error: Could not connect to OVM Manager")
        sys.exit(1)
