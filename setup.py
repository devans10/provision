from setuptools import setup, find_packages

with open('README.rst', encoding='UTF-8') as f:
    readme = f.read()

setup(
    name='provision',
    version='0.1.0',
    description='Utility to provision multiple VMs from JSON property file input',
    long_description=readme,
    author='Dave Evans',
    author_email='devans@fastmail.com',
    packages=find_packages('src'),
    package_dir={'': 'src'},
    install_requires=[],
    entry_points={
        'console_scripts': [
            'provision=provision.cli:main',
        ],
    }
)
